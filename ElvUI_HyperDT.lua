local E, L, V, P, G, _ = unpack(ElvUI)
local HDT = E:NewModule('HyperDT')
local DT = E:GetModule('DataTexts')

E.HyperDT = HDT

local datatexts = {}
local datatextMap = {}
local menu = {}
local chosenDT = nil
local menuFrame = CreateFrame("Frame", "HyperDTMenuFrame", E.UIParent)

function HDT:EnableHyperModeForDataText(dt)
	if not dt.overlay then return end
	dt.oldClick = dt:GetScript('OnClick')
	dt.oldEnter = dt:GetScript('OnEnter')
	dt.oldLeave = dt:GetScript('OnLeave')
	dt:SetScript('OnEnter',nil)
	dt:SetScript('OnLeave',nil)
	dt:SetScript('OnClick',function(self,button)
		if button == "RightButton" then
			chosenDT = dt
			menuFrame.point = 'CENTER'
			menuFrame.relativePoint = 'CENTER'
			E:DropDown(menu, menuFrame);
		end
	end)
end

function HDT:DisableHyperModeForDataText(dt)
	if not dt.overlay then return end
	dt:SetScript('OnClick',dt.oldClick)
	dt:SetScript('OnEnter',dt.oldEnter)
	dt:SetScript('OnLeave',dt.oldLeave)
	dt.oldClick = nil
	dt.oldEnter = nil
	dt.oldLeave = nil
end

function HDT:EnableHyperMode()
	for datatext, _ in pairs(datatexts) do
		self:EnableHyperModeForDataText(datatext)
		datatext.overlay:Show()
	end
end

function HDT:DisableHyperMode()
	for datatext, _ in pairs(datatexts) do
		self:DisableHyperModeForDataText(datatext)
		datatext.overlay:Hide()
	end
end

function HDT:Initialize()
	menuFrame:SetTemplate("Transparent", true)
	for name, _ in pairs(DT.RegisteredDataTexts) do
		tinsert(menu,{ text = name, func = function() chosenDT.menuSetFunc(name) end })
	end
	sort(menu, function(a,b) return a.text < b.text end)
	tinsert(menu,{ text = 'None', func = function() chosenDT.menuSetFunc(NONE) end })
	
	for panelName, panel in pairs(DT.RegisteredPanels) do
		--Restore Panels
		for i=1, panel.numPoints do
			local pointIndex = DT.PointLocation[i]
			panel.dataPanels[pointIndex].overlay = panel.dataPanels[pointIndex]:CreateTexture(nil, 'OVERLAY')
			panel.dataPanels[pointIndex].overlay:SetTexture(0,1,0,.3)
			panel.dataPanels[pointIndex].overlay:SetAllPoints()
			panel.dataPanels[pointIndex].overlay:Hide()
			panel.dataPanels[pointIndex].menuSetFunc = function(value)
				if string.find(datatextMap[panel.dataPanels[pointIndex]],"::") then
					local option,pointIndex = string.match(datatextMap[panel.dataPanels[pointIndex]],"([%w_]+)::([%w_]+)")
					DT.db.panels[option][pointIndex] = value
				else
					local option = datatextMap[panel.dataPanels[pointIndex]]
					DT.db.panels[option] = value
				end
				DT:LoadDataTexts(); 
				self:EnableHyperMode() 
			end
			for option, value in pairs(DT.db.panels) do
				if value and type(value) == 'table' then
					if option == panelName and DT.db.panels[option][pointIndex] then
						datatextMap[panel.dataPanels[pointIndex]] = option.."::"..pointIndex
					end
				elseif value and type(value) == 'string' then
					if option == panelName then
						datatextMap[panel.dataPanels[pointIndex]] = option
					end
				end
			end
			datatexts[panel.dataPanels[pointIndex]] = true
		end
	end

	SLASH_HYPERDT1 = '/hdt';
	local hdt_enabled = false
	function SlashCmdList.HYPERDT(msg, editbox)
		if hdt_enabled then
			hdt_enabled = false
			HDT:DisableHyperMode()
		else
			hdt_enabled = true
			HDT:EnableHyperMode()
		end
	end
end

E:RegisterModule(HDT:GetName())		